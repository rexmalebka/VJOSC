import cv2
import numpy as np
import imutils
import time
from numba import jit
import math

img = cv2.imread('imagen.jpg')


def f(img, N=6):
    w,h = img.shape[:2]
    
    if w%2== 0:
        w += 1
    if h%2==0:
        h += 1
    
    #img = cv2.resize(img,(h,w))
    
    total = np.zeros(img.shape, dtype=np.uint8)
    """
    d1 = math.tan( (180/N)*math.pi/180 ) * h
    dx = int((w - d1)/2)
    triangle = np.array([[dx,0], [w-dx,0], [w/2, h/2], [dx,0]], dtype=np.int32)  
    triangle = cv2.fillConvexPoly(np.zeros((w,h)),cv2.convexHull(triangle), 255 ).astype(np.uint8)
    mask = cv2.bitwise_and(img, img, mask = triangle)
    for k in range(0, N):
        m = imutils.rotate(triangle, angle=k*(360/N), center= (w/2, h/2))
        res = imutils.rotate(mask, angle=k*(360/N), center=(w/2, h/2))
        total[np.nonzero(m)] = res[np.nonzero(m)]
    total = cv2.dilate(total, np.ones((3,3)))
    """
    return img

t1 = time.time()
total = f(img)
t2 = time.time()


print('tiempo total : ', t2-t1)

cv2.imshow('tot',total)
cv2.waitKey(-1)

cv2.destroyAllWindows()
