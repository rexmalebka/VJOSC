from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.dispatcher import Dispatcher
import asyncio
import cv2
import numpy as np

video = cv2.VideoCapture('video.mp4')

class GlitchVideo():

    def __init__(self, url: str) -> None:
        self._url = url
        self._videoCapture = cv2.VideoCapture(url)

        self._fps = int(1000 / self._videoCapture.get(cv2.CAP_PROP_FPS))
        self._total = self._videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)
        self._current = 0
        
        self._rel = False
        self._frame = np.zeros((1,1,1),dtype=np.uint8)

        self._width = self._videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)
        self._height = self._videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)
        
        self._pause = False
        self._limlow = 0
        self._limupp = self._total - 1 

        self._geomTransf = GeomTransf(self)

    def pause(self, *args: tuple) -> None:
        print(args)
        self._pause = not self._pause

    def fps(self, *args: tuple) -> None:
        self._fps = int(1000 / args[1] )
    
    def jump(self, *args: tuple) -> None:
        nframe = args[1]
        self.videoCapture.set(CV2_CAP_PROP_POS_FRAMES, nframe)

    def repeat(self, *args: tuple):
        start = args[1]
        end = args[2]

        self._limlow = start
        self._limupp = end

        self._videoCapture.set(cv2.CAP_PROP_POS_FRAMES, start)
        self._current = start

    async def loop(self):
        # main loop

        self._rel, self._frame = self._videoCapture.read()

        while self._rel:

            if not self._pause:
                cv2.imshow('frame', self._frame)
                self._rel, self._frame = self._videoCapture.read()
                self._current += 1

            cv2.waitKey(self._fps)
            
            if self._current == self._limupp - 1:
                self._videoCapture.set(cv2.CAP_PROP_POS_FRAMES, self._limlow)
                self._current = self._limlow   

            if self._current == self._total - 1:
                self._videoCapture.set(cv2.CAP_PROP_POS_FRAMES, 0)
                self._current = 0    

            await asyncio.sleep(0.01)

class GeomTransf():

    def __init__(self, glitchObj: GlitchVideo) -> None:
        self._glitchObj = glitchObj
        self._functions = {}
        self._effects = []

    def parse(self, *args): 
        list_fx = args[1:]
        effects = []

        for effect in list_fx:
            dict_fx = {'name': effect[0]}
            print(self._functions,[dict_fx['name']])
            dict_fx.update({k:l for k,l in zip(effect[1::2], effect[2::2])})
            effects.append(dict_fx)

        self._effects = effects
        print(self._effects)

    def register(name):
            
    # @register(name = "scale")
    def scale(self, size):
        print(self._functions)
        return cv2.Resize(
                self._glitchObj, 
                (
                    int(self.geomSelf._width * value),
                    int(self._height * value)
                    )
                )
        
glitchobj = GlitchVideo('video.mp4')

dispatcher = Dispatcher()
dispatcher.map("/pause", glitchobj.pause)
dispatcher.map("/repeat", glitchobj.repeat)
dispatcher.map("/fps", glitchobj.fps)
dispatcher.map("/geom", glitchobj._geomTransf.parse)


ip = "127.0.0.1"
port = 1337

async def init_main():
    server = AsyncIOOSCUDPServer((ip, port), dispatcher, asyncio.get_event_loop())
    transport, protocol = await server.create_serve_endpoint()  # Create datagram endpoint and start serving
    await glitchobj.loop()  # Enter main loop of program
    transport.close()  # Clean up serve endpoint

asyncio.run(init_main())
