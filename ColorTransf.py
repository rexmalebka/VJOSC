import cv2
import numpy as np

class _ColorTransf():
    def __init__(self):
        self.effects = {}
        self._vjObj = None
        self._appliedEffects = []

    def add(self, name):
        name = name.lower()

        def wrapper1(func):

            def wrapper2(frame,*args, **kargs):
                if frame is not None:
                    return func(self._vjObj._frame, **kargs)

            self.effects.update({name: wrapper2})
            print(">> registered function {}".format(name))

            return wrapper2

        return wrapper1

ColorTransf = _ColorTransf()

@ColorTransf.add(name='blur')
def blur(frame, val):
    return frame

@ColorTransf.add(name='contrast')
def contrast(frame,val=1):
    return cv2.convertScaleAbs(frame, alpha=val)

@ColorTransf.add(name='brightness')
def brightness(frame,val=1):
    return cv2.convertScaleAbs(frame, beta=val)

@ColorTransf.add(name='dilate')
def dilate(frame, dx=0, dy=0, it=1):
    return cv2.dilate(frame, np.ones((dx,dy),np.uint8),iterations=1)

@ColorTransf.add(name='erode')
def erode(frame, dx=0, dy=0, it=1):
    return cv2.erode(frame, np.ones((dx,dy),np.uint8),iterations=1)



@ColorTransf.add(name='invert')
def invert(frame):
    return 255-frame
