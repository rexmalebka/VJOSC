import cv2
import numpy as np
import math
import imutils
from numba import jit
import time

class _GeomTransf():
    def __init__(self):
        self.effects = {}
        self._vjObj = None
        self._appliedEffects = []

    def add(self, name):
        name = name.lower()

        def wrapper1(func):

            def wrapper2(**kargs):
                t1 = time.time()
                self._vjObj._frame = func(self._vjObj._frame, **kargs)
                t2 = time.time()

                print('tiempo: ',t2-t1)
                return None

            self.effects.update({name: wrapper2})
            print(">> registered function {}".format(name))

            return wrapper2

        return wrapper1

GeomTransf = _GeomTransf()

@GeomTransf.add(name='scale')
def scale(frame, val=1):
    w = frame.shape[0]
    return imutils.resize(frame, width=int(w*val))

@GeomTransf.add(name = 'rotate')
def rotate(frame, val):
    return imutils.rotate(frame, angle=val)

@GeomTransf.add(name='flipX')
def flipX(frame):
    return cv2.flip(frame,1)

@GeomTransf.add(name='flipY')
def flipY(frame):
    return cv2.flip(frame,0)

@GeomTransf.add(name='kaleid')
def kaleid(frame, N=6):
    w,h = frame.shape[:2]

    if w%2== 0:
        w += 1
    if h%2==0:
        h += 1
    
    frame = cv2.resize(frame, (h,w))
    
    total = np.zeros(frame.shape, dtype=np.uint8)
    
    d1 = math.tan( (180/N)*math.pi/180 ) * h
    dx = int((w - d1)/2)
    triangle = np.array([[dx,0], [w-dx,0], [w/2, h/2], [dx,0]], dtype=np.int32)  
    triangle = cv2.fillConvexPoly(np.zeros((w,h)),cv2.convexHull(triangle), 255 ).astype(np.uint8)
    mask = cv2.bitwise_and(frame, frame, mask = triangle)
    for k in range(0, N):
        m = imutils.rotate(triangle, angle=k*(360/N), center= (w/2, h/2))
        res = imutils.rotate(mask, angle=k*(360/N), center=(w/2, h/2))
        total[np.nonzero(m)] = res[np.nonzero(m)]
    frame = cv2.dilate(total, np.ones((3,3)))
    
    return frame

@GeomTransf.add(name='translate')
def translate(frame,dx =0, dy=0):
    return imutils.translate(frame,dx,dy)

@GeomTransf.add(name='sort')
def fsort(frame):
    return np.sort(frame,axis=0)

@GeomTransf.add(name='unsort')
def unsort(frame):
    np.random.shuffle(frame)
    return frame
