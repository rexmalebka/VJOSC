import cv2

video = cv2.VideoCapture("video.mp4")

fps = 1000 / video.get(cv2.CAP_PROP_FPS)

video.set(cv2.CAP_PROP_POS_AVI_RATIO, 1) # jump to end

video_time = video.get(cv2.CAP_PROP_POS_MSEC)

rel = True

while rel:

    video_time -= fps;
    video.set(cv2.CAP_PROP_POS_MSEC, video_time);
    
    rel, frame = video.read()
    cv2.imshow('frame', frame)
    cv2.waitKey(int(1000/100))

    if video.get(cv2.CAP_PROP_POS_MSEC) == 1:
        video.set(cv2.CAP_PROP_POS_AVI_RATIO,1)
        video_time = video.get(cv2.CAP_PROP_POS_MSEC)

cv2.destroyAllWindows()
