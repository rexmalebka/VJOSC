"""Small example OSC client

This program sends 10 random values between 0.0 and 1.0 to the /filter address,
waiting for 1 seconds between each value.
"""
from pythonosc import osc_bundle_builder
from pythonosc import osc_message_builder
from pythonosc import udp_client
import time

ip = "127.0.0.1"
port = 5005
client = udp_client.SimpleUDPClient(ip, port)

fx = [['scale','val',2],['flipx'],['flipy']]

print('client.send_message("/fps", 1)')
#client.send_message("/fx", fx)
