from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.dispatcher import Dispatcher
import asyncio
import cv2
import numpy as np
from GeomEffects import GeomTransf
from ColorEffects import ColorTransf

class Server():
    def __init__(self, ip: str, port: int):
        self._ip = ip
        self._port = port

        self._Dispatcher = Dispatcher()

        self.player = self._Player(self, '')

        GeomTransf._vjObj = self.player
        self.GeomTransf = GeomTransf        
        
        ColorTransf._vjObj = self.player
        self.ColorTransf = ColorTransf        

    def start(self):
        print(">> starting server on {}:{}".format(self._ip, self._port))
        asyncio.run(self.init_main())
        
    async def init_main(self):
        self.server = AsyncIOOSCUDPServer(
                (self._ip, self._port),
                self._Dispatcher, 
                asyncio.get_event_loop()
                )

        transport, protocol = await self.server.create_serve_endpoint() 
        await self.loop()  
        transport.close()  

    async def loop(self):

        while not self.player._stop:
            self.player.apply()
            
            if not self.player._pause:
                self.player.play()
                cv2.waitKey(self.player._fps)
            
            await asyncio.sleep(0.001)

    class _Player(object):
        def __init__(self, cls, url : str) -> None:
            self.cls = cls
            
            self._url = ""
            self._videoCapture = cv2.VideoCapture(url)
            
            self._current = 0
            self._rel = False
            self._frame = np.zeros((1,1,3))

            self._width = self._videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)
            self._height = self._videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)
            
            self._fps = int(1000 / (self._videoCapture.get(cv2.CAP_PROP_FPS) + 1))
            
            self._pause = False
            self._stop = False


            self.cls._Dispatcher.map('/load', self.load)
            self.cls._Dispatcher.map('/pause', self.pause)
            self.cls._Dispatcher.map('/stop', self.stop)
            self.cls._Dispatcher.map('/fx', self.fx)
            self.cls._Dispatcher.map('/clean', self.clean)
            self.cls._Dispatcher.map('/speed', self.speed)
            self.cls._Dispatcher.map('/loop', self.loop)

        def load(self,*args):
            url = args[1]
            self._url = url
            print(">> adding video '{}'".format(url))
            self._videoCapture = cv2.VideoCapture(url)
            self._fps = int(1000 / (self._videoCapture.get(cv2.CAP_PROP_FPS) + 1))

        def apply(self):
            for f in self.cls.GeomTransf._appliedEffects:
                func = self.cls.GeomTransf.effects[f[0]]
                func(**f[1])
            cv2.imshow('frame', self._frame)
            
            #for f in self.cls.ColorTransf._appliedEffects:
            #    func = self.cls.ColorTransf.effects[f[0]]

        def play(self):
            if not self._pause:
                self._rel, self._frame = self._videoCapture.read()
                
                if self._rel:
                    self._current += 1
                else: 
                    self._videoCapture.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    self._current = 0
                    self._rel, self._frame = self._videoCapture.read()
                    
                if not self._rel:
                    self._frame = np.zeros((1,1,3))

           
        def pause(self, *args):
            self._pause = not self._pause
            cv2.imshow('frame', self._frame)

        def stop(self, *args):
            self._stop = True

        def jump(self):
            pass        
        
        def speed(self,*args):
            ratio = args[1]
            self._fps = int(ratio * self._fps)

        def fx(self, com, *args):
            geomeffects = []
            coloreffects = []

            for unparsed in args:
                name = unparsed[0]
                k = unparsed[1::2]
                v = unparsed[2::2]

                if name in self.cls.GeomTransf.effects:
                    geomeffects.append([name,{key:val for key,val in zip(k,v)}])

                if name in self.cls.ColorTransf.effects:
                    coloreffects.append([name,{key:val for key,val in zip(k,v)}])

            if len(geomeffects)>0:
                self.cls.GeomTransf._appliedEffects = geomeffects
            if len(coloreffects)>0:
                self.cls.ColorTransf._appliedEffects = coloreffects

        def clean(self, *args):
            self.cls.GeomTransf._appliedEffects = []
            self.cls.ColorTransf._appliedEffects = []

        def loop(self, *args):
            pass


