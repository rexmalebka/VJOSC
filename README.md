# OSC vjing with opencv in python

async OSC server for opencv based VJing.

still on progress I'm open to ideas.

`main.py` has all my progress.

`client.py` is the client.

`GeomTransf.py` contains geometric functions.

`ColorTransf.py` contains color transformation functions.

![Cat meowing](preview.png "Cat meowing test")


## Requirements

* pythonosc
* opencv
* python3.7
* numpy
* imutils

## Maybe in the future

- look for time and memory improvements.
- do some documentations.
- ask friends for improvements.
- do a better interface

_Geometric_:

- Scale
- Rotate
- FlipX
- FlipY
- Kaleid
- Translate

_Color_:

- Opacity
- Brightness
- Contrast
- Blur
- Canny
